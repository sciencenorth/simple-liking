json.array!(@votes) do |vote|
  json.extract! vote, :id, :exhibit_id, :count
  json.url vote_url(vote, format: :json)
end
